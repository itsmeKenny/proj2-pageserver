from flask import Flask, render_template, request 
import os
app = Flask(__name__)

@app.route("/<path:subpath>")
def hello(subpath):
    name = subpath
    source_path = os.getcwd()
    newname = request.environ['REQUEST_URI'] #checks full url
    fileexist = os.path.exists(source_path + "/templates" + newname) #checks if file exists in templates folder
    ###For files inside subdirectories
    if "//" in name:
        return error_403(name)
    if "~" in name:
        return error_403(name)
    if ".." in name:
        return error_403(name)
    ###

    ###Section for checking single files
    if newname.startswith(("/..","/~","//")): #If name contains forbidden char
        return error_403(name)
    if name.endswith((".html", ".css")): #If name is a css or html then go
        if fileexist:
            return render_template(name), 200
        else:
            return error_404(name) #File not in DOCROOT
    else:
        return error_404(name) #FILE not a html or css
    ###

@app.errorhandler(404)
def error_404(error):
    #return a template with error code
    return render_template("404.html"), 404

@app.errorhandler(403)
def error_403(error):
    #return a template with error code
    return render_template("403.html"), 403
if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
